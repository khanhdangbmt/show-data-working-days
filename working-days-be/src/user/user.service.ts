import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './schema/user.schema';
import { Model } from 'mongoose';
import { ApiTags } from '@nestjs/swagger';

@Injectable()
@ApiTags('Users')
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}

  async findAll({ index = 0, limit = 10 }: { index?: number; limit?: number }) {
    const [result, totalRecords] = await Promise.all([
      this.userModel.aggregate([
        { $skip: parseInt(index * limit + '') },
        { $limit: parseInt(limit + '') },
        {
          $lookup: {
            from: 'meetings',
            localField: 'id',
            foreignField: 'user_id',
            as: 'meeting_days',
          },
        },
        {
          $project: {
            id: 1,
            first_name: 1,
            last_name: 1,
            email: 1,
            gender: 1,
            days: 1,
            meeting_days: 1,
            total: 1,
          },
        },
      ]),
      this.userModel.aggregate([{ $count: 'total' }]),
    ]);

    const mappedResult = result.map((user) => {
      const arrDays = new Array(user.days).fill('');
      user.meeting_days.forEach((meeting) => {
        arrDays[meeting.start_day - 1] += 's';
        arrDays[meeting.end_day - 1] += 'e';
      });

      let flag = 0;
      let countEmpty = 0;

      function countChar(str: string, char: string): number {
        const regex = new RegExp(char, 'g');
        return (str.match(regex) || []).length;
      }

      arrDays.forEach((dayFlag) => {
        const countStart = countChar(dayFlag, 's');
        const countEnd = countChar(dayFlag, 'e');

        // start and end in one day
        if (countEnd !== 0 && countStart !== 0 && countStart === countEnd) {
          return;
        }

        if (countStart > 0) {
          flag += countStart;
        }

        if (countEnd > 0) {
          flag -= countEnd;
        }

        if (flag === 0 && countStart === 0 && countEnd === 0) {
          countEmpty += 1;
        }
      });

      return {
        ...user,
        days_without_meetings: countEmpty,
      };
    });

    return {
      docs: mappedResult,
      paging: {
        index: +index,
        limit: +limit,
        hasNext: Boolean(totalRecords[0].total >= (+index + 1) * +limit),
      },
    };
  }
}
