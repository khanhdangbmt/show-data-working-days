import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { UserEntity } from '../entities/user.entity';
import { Meeting } from 'src/meeting/schema/meeting.schema';

export type UserDocument = HydratedDocument<User>;

export enum UserGender {
  male = 'male',
  female = 'female',
  other = 'other',
}

@Schema({ _id: false, timestamps: true })
export class User implements UserEntity {
  @Prop({ type: 'Number', unique: true })
  id: number;

  @Prop()
  first_name: string;

  @Prop()
  last_name: string;

  @Prop()
  email: string;

  @Prop({ enum: UserGender })
  gender: UserGender;

  @Prop()
  ip_address: string;

  @Prop({ min: 0, max: 50, type: 'Number' })
  days: number;
}

export const UserSchema = SchemaFactory.createForClass(User);
