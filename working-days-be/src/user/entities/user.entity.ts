export class UserEntity {
  id: number; //(integer, primary key): Unique identifier assigned to each employee.
  first_name: string; // (varchar): First name of the employee.
  last_name: string; // (varchar): Last name of the employee.
  email: string; // (varchar): Email address associated with the employee for communication and identification purposes.
  gender: string; // (varchar): Gender of the employee, commonly represented as 'male', 'female', or 'other'.
  ip_address: string; // (varchar): IP address attributed to the employee.
  days: number; // (integer): Total number of days the employee is available for work, starting from day 1 of their availability.
}
