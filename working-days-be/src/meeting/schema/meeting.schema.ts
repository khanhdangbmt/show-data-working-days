import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { MeetingEntity } from '../entities/meeting.entity';
import { User } from 'src/user/schema/user.schema';

export type MeetingDocument = HydratedDocument<Meeting>;

export enum MeetingGender {
  male = 'male',
  female = 'female',
  other = 'other',
}

@Schema({ _id: false, timestamps: true })
export class Meeting implements MeetingEntity {
  @Prop({ type: 'Number' })
  id: number;

  @Prop({ type: 'Number', ref: 'users' })
  user_id: number;

  @Prop({ type: 'Number' })
  room_id: number;

  @Prop({ type: 'Number' })
  start_day: number;

  @Prop({ type: 'Number' })
  end_day: number;
}

export const MeetingSchema = SchemaFactory.createForClass(Meeting);
