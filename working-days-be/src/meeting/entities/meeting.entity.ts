export class MeetingEntity {
  id: number; // (integer, primary key): Unique identifier for each attendance record.
  user_id: number; // (integer): Identifier referencing the user who attended the meeting.
  room_id: number; // (integer): Identifier referencing the room where the meeting took place.
  start_day: number; // (integer): The day on which the meeting starts.
  end_day: number; // (integer): The day on which the meeting ends.
}
