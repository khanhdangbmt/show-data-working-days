"use client";

import { Table, Tag } from "antd";
import { useQuery } from "@tanstack/react-query";
import { useEffect, useState } from "react";
import qs from "query-string";

interface Meeting {
  _id: string;
  id: number;
  start_day: number;
  end_day: number;
  user_id: number;
  room_id: number;
}

interface UserMeetings {
  _id: string;
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  gender: string;
  days: number;
  meeting_days: Meeting[];
  days_without_meetings: number;
}

const headers = new Headers({
  Authorization: `Bearer ${process.env.VERCEL_ACCESS_TOKEN}`,
  Accept: "application/json",
  "Content-Type": "application/json",
});

export default function Home() {
  const [tableData, setTableData] = useState<UserMeetings[]>();
  const [hasData, setHasData] = useState<boolean>(true);
  const [filterParams, setFilterParams] = useState({
    index: 0,
    limit: 10,
  });

  const fetchList = async () => {
    const filter = qs.stringify(filterParams);
    const data = await fetch(process.env.BE_URL + "user?" + filter, {
      method: "GET",
      headers,
    });
    return await data.json();
  };

  const dataQuery = useQuery({
    queryKey: ["get-list-users", { ...filterParams }],
    queryFn: () => fetchList(),
  });

  useEffect(() => {
    setTableData((prev) => [...(prev || []), ...(dataQuery?.data?.docs || [])]);
    if (dataQuery?.data?.paging !== undefined) {
      setHasData(dataQuery?.data?.paging?.hasNext);
    }
  }, [dataQuery?.data?.docs, dataQuery?.data?.paging]);

  const columns = [
    {
      title: "#",
      width: 50,
      render: (_field: string, _record: UserMeetings, index: number) => {
        return <div>{index + 1}</div>;
      },
    },
    {
      title: "First name",
      dataIndex: "first_name",
      key: "first_name",
      width: 150,
      isDefault: true,
      fixed: true,
    },
    {
      title: "Last name",
      dataIndex: "last_name",
      key: "last_name",
      width: 150,
      isDefault: true,
      fixed: true,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      width: 230,
      isDefault: true,
    },
    {
      title: "Gender",
      dataIndex: "gender",
      key: "gender",
      width: 100,
      isDefault: true,
    },
    {
      title: "Days",
      dataIndex: "days",
      key: "days",
      width: 100,
      isDefault: true,
    },
    {
      title: "Meeting days",
      dataIndex: "days",
      key: "days",
      width: 150,
      isDefault: true,
      render: (_field: number, record: UserMeetings) => {
        return (
          <div key={record._id}>
            {record.meeting_days.map((meeting: any, index: number) => {
              const color = ["magenta", "lime", "volcano", "orange", "gold"];
              const actualColor: string =
                color[Math.ceil(meeting.start_day / 10)];
              return (
                <div key={index + record._id} className="mb-2">
                  <Tag color={actualColor} className="block">
                    {`${meeting.start_day} -> ${meeting.end_day}`}{" "}
                  </Tag>
                </div>
              );
            })}
          </div>
        );
      },
    },
    {
      title: "Days without meeting",
      dataIndex: "days_without_meetings",
      key: "days_without_meetings",
      width: 150,
      isDefault: true,
    },
  ];

  return (
    <div className="p-2">
      <h1 className="text-black">User working days</h1>

      <Table
        className="w-full h-full"
        scroll={{
          x: 600,
          y: "calc(100vh - 17rem)",
        }}
        onScroll={(event: any) => {
          const { scrollTop, scrollHeight, clientHeight } = event.target;
          const isEndOfTable =
            Math.floor(scrollTop) + clientHeight >= scrollHeight;

          console.log("🚀 ~ Home ~ hasData:", hasData);
          if (isEndOfTable && hasData) {
            setFilterParams((prev) => ({ ...prev, index: prev.index + 1 }));
            // Handle end-of-table scenario (e.g., load more data)
          }
        }}
        pagination={false}
        columns={columns}
        dataSource={tableData || []}
        loading={dataQuery.isFetching}
      />
    </div>
  );
}
