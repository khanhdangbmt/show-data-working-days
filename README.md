# Show Data Working Days

## Getting started

**Running BE:**

1. `pnpm i`
2. Create a `.env` file
    * `CLIENT_ORIGINS= // split by comma`
    * `MONGO_URI=`
3. `pnpm run start:dev`

**Running FE:**

1. `pnpm i`
2. `pnpm run dev`

## Assumptions for the system

### BE

* Uses MongoDB with 2 collections: user, meeting
* Omits authentication, authorization, unit testing, sorting, etc.
* Focuses on getting a list with the calculation of `days_without_meeting`

### FE

* Omits authentication, authorization, unit testing, sorting, etc.
* Focuses on handling loading new data when the user scrolls down to the end of the table


### Result
**FE**
![alt text](image.png)
**BE**
![alt text](image-1.png)